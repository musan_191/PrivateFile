CREATE PROCEDURE [dbo].[Proc_ReportDepProcDaily] 
	@Flag int = 0, --0-班次汇总，1-工作站汇总
	@StartDate nvarchar(20),
	@EndDate nvarchar(20),
	@Time1_S nvarchar(5),--白班开始
	@Time1_E nvarchar(5),--白班结束
	@Time2_S nvarchar(5),--晚班开始
	@Time2_E nvarchar(5),--晚班结束
	@DepId int
AS

if @Flag=0
begin

	--已下达制令单的首末工序
	select max(b.DepName) DepName , a.PFormMId , max(PFormMNo) PFormMNo , min(ProcessNo) FirstProcessNo , max(ProcessNo)						LastProcessNo into #tmp1
	FROM ProProcess a left JOIN ProduceFormM b ON a.PFormMId=b.PFormMId 
	left JOIN Department c ON b.DepName=c.DepName
	where b.Issued=3 and c.DepId=@DepId
	GROUP BY a.PFormMId
	
	--select * from #tmp1

	IF object_id(N'tempdb..#tmpDep') IS NOT NULL
    DROP TABLE #tmpDep
	CREATE table #tmpDep
	(
		Class nvarchar(10) NULL, 
		InputQty DECIMAL(12,4) NULL, 
		OutputQty DECIMAL(12,4) NULL
	)
	declare @inputNum DECIMAL(12,4),@outputNum DECIMAL(12,4) 
	--白班 首工序的报工单
	SELECT @inputNum=isnull(sum(GenuineNum),0) FROM WorkRecord aa
	INNER JOIN #tmp1 bb ON aa.PFormMNo=bb.PFormMNo AND aa.ProcNo=bb.FirstProcessNo
	where ReportTime between @StartDate and @EndDate 
	and datediff(minute,@Time1_S,CONVERT(char(5), ReportTime, 8))>=0 and datediff(minute,CONVERT(char(5), ReportTime, 8) , @Time1_E)>0
	--白班 末工序的报工单
	SELECT @outputNum=isnull(sum(GenuineNum),0) FROM WorkRecord aa
	INNER JOIN #tmp1 bb ON aa.PFormMNo=bb.PFormMNo AND aa.ProcNo=bb.LastProcessNo
	where ReportTime between @StartDate and @EndDate 
	and datediff(minute,@Time1_S,CONVERT(char(5), ReportTime, 8))>=0 and datediff(minute,CONVERT(char(5), ReportTime, 8) , @Time1_E)>0
	
	insert INTO #tmpDep SELECT '白班',@inputNum,@outputNum
	
	--晚班 首工序的报工单 
	SELECT @inputNum=isnull(sum(GenuineNum),0) FROM WorkRecord aa
	INNER JOIN #tmp1 bb ON aa.PFormMNo=bb.PFormMNo AND aa.ProcNo=bb.FirstProcessNo
	where ReportTime between @StartDate and @EndDate 
	and datediff(minute,@Time2_S,CONVERT(char(5), ReportTime, 8))>=0 and datediff(minute,CONVERT(char(5), ReportTime, 8) , @Time2_E)>0
	--晚班 末工序的报工单
	SELECT @outputNum=isnull(sum(GenuineNum),0) FROM WorkRecord aa
	INNER JOIN #tmp1 bb ON aa.PFormMNo=bb.PFormMNo AND aa.ProcNo=bb.LastProcessNo
	where ReportTime between @StartDate and @EndDate 
	and (datediff(minute,@Time2_S,CONVERT(char(5), ReportTime, 8))>=0 or datediff(minute,CONVERT(char(5), ReportTime, 8) , @Time2_E)>0)
	
	insert INTO #tmpDep SELECT '晚班',@inputNum,@outputNum
	
	select * from #tmpDep
	
end
else if @Flag=1
begin
	
	select a.CenterId,max(b.CenterName) CenName,
	sum(isnull(a.GenuineNum,0)) OutputQty,
	sum(isnull(a.DefectNum,0))+sum(isnull(c.ScrapNum,0)) BadQty ,
	sum(isnull(a.GenuineNum,0))/(sum(isnull(a.GenuineNum,0))+sum(isnull(a.DefectNum,0))+sum(isnull(c.ScrapNum,0))) GoodRate,
	sum(isnull(bb.InputNum,0))-sum(isnull(bb.OutputNum,0))  WIPQty
	from WorkRecord a
	left JOIN ProcessWork bb on a.ProWorkId=bb.ProWorkId
	left JOIN WorkCenter b on a.CenterId=b.CenterId
	left JOIN InspRecord c ON a.InspId=c.InspId
	where ReportTime between @StartDate and @EndDate 
	group by a.CenterId
	order BY a.CenterId
	
end